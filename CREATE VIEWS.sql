IF OBJECT_ID ('CompaniesToBePhoned', 'V') IS NOT NULL
    DROP VIEW CompaniesToBePhoned;
GO
CREATE VIEW CompaniesToBePhoned
AS
	SELECT C.CustomerID, C.Phone
	FROM Customer AS C
	JOIN Orders AS O
		ON O.CustomerID = C.CustomerID AND O.Canceled = 0
		AND O.PlacesOrdered !=(SELECT dbo.getTotalOrderHeadCount(O.OrderID))
	JOIN Event AS E
		ON O.EventID = E.EventID AND E.Canceled = 0
	JOIN Conference AS Conf
		ON E.Canceled = Conf.ConferenceID 
		AND (DATEDIFF(day, GETDATE(), Conf.Starts) <= 14)
	WHERE C.CustomerID = 1
GO

IF OBJECT_ID ('OrdersToBeCanceled', 'V') IS NOT NULL
    DROP VIEW OrdersToBeCanceled;
GO
CREATE VIEW OrdersToBeCanceled
AS
	SELECT O.OrderID, O.CustomerID
	FROM Orders AS O
	JOIN Payment AS P
		ON O.OrderID = P.OrderID AND O.Canceled = 0
		AND (DATEDIFF(day, GETDATE(), O.OrderDate) >= 7)
	GROUP BY O.OrderID, O.CustomerID
	HAVING SUM(P.Amount) < (SELECT dbo.getTotalOrderPrice(O.OrderID))
GO

IF OBJECT_ID ('TopCustomers', 'V') IS NOT NULL
    DROP VIEW TopCustomers;
GO
CREATE VIEW TopCustomers
AS
	SELECT TOP 7 C.Login, COUNT(O.OrderID) AS 'TotalOrders', SUM(P.Amount) AS 'TotalPaid', SUM(TP.TotalPrice) AS 'TotalPrice'
	FROM Customer AS C
	LEFT JOIN Orders AS O
		ON C.CustomerID = O.CustomerID AND O.Canceled = 0
	LEFT JOIN (SELECT OI.OrderID AS 'OrderID', dbo.getTotalOrderPrice(OI.OrderID) AS 'TotalPrice' FROM Orders AS OI) AS TP
		ON TP.OrderID = O.OrderID
	JOIN Payment AS P
		ON P.Amount = O.OrderID
	GROUP BY C.Login
	ORDER BY TotalPaid DESC
GO

IF OBJECT_ID ('TopWorkshop', 'V') IS NOT NULL
    DROP VIEW TopWorkshop;
GO
CREATE VIEW TopWorkshop
AS
	SELECT TOP 14 W.EventID, W.Name, E.TotalPlaces, SUM(O.PlacesOrdered) AS 'TotalOrdered'
	FROM Workshop AS W
	JOIN Event AS E
		ON W.EventID = E.EventID AND E.Canceled = 0
	JOIN Orders AS O
		ON W.EventID = O.EventID AND O.Canceled = 0
	GROUP BY W.EventID, W.Name, E.TotalPlaces
	ORDER BY TotalOrdered DESC
GO

IF OBJECT_ID ('TopConference', 'V') IS NOT NULL
    DROP VIEW TopConference;
GO
CREATE VIEW TopConference
AS
	SELECT TOP 3 C.Name, COUNT(E.EventID) AS 'TotalEvents',
		SUM(O.PlacesOrdered) AS 'TotalOrderedPlaces'
	FROM Conference AS C
	JOIN Event AS E
		ON E.ConferenceID = C.ConferenceID AND E.Canceled = 0
	JOIN Orders AS O
		ON E.EventID = O.OrderID
	GROUP BY C.ConferenceID, C.Name
	ORDER BY TotalOrderedPlaces DESC
GO

IF OBJECT_ID ('TrancationsToBeMade', 'V') IS NOT NULL
    DROP VIEW TrancationsToBeMade;
GO
CREATE VIEW TrancationsToBeMade
AS
	SELECT OrderID, (SELECT dbo.getOrderSaldo(OrderID)) AS 'Amount'
	FROM Payment
	WHERE (SELECT dbo.getOrderSaldo(OrderID)) < 0
GO
