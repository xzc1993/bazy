USE kzieba_a;

ALTER TABLE Event DROP CONSTRAINT FK_Event_Conference;
ALTER TABLE ConferenceDay DROP CONSTRAINT FK_ConferenceDay_Event;
ALTER TABLE Workshop DROP CONSTRAINT FK_Workshop_Event;
ALTER TABLE Workshop DROP CONSTRAINT FK_Workshop_ConferenceDay;
ALTER TABLE Prices DROP CONSTRAINT FK_Prices_ConferenceDay;
ALTER TABLE Orders DROP CONSTRAINT FK_Orders_Event;
ALTER TABLE Orders DROP CONSTRAINT FK_Orders_Customer;
ALTER TABLE OrderDetails DROP CONSTRAINT FK_OrderDetails_Participant;
ALTER TABLE OrderDetails DROP CONSTRAINT FK_OrderDetails_Orders;
ALTER TABLE Payment DROP CONSTRAINT FK_Payment_Orders;

DROP TABLE Conference;
DROP TABLE ConferenceDay;
DROP TABLE Customer;
DROP TABLE Event;
DROP TABLE Participant;
DROP TABLE Prices;
DROP TABLE OrderDetails;
DROP TABLE Orders;
DROP TABLE Workshop;
DROP TABLE Payment;