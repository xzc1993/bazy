CREATE TABLE Conference
(
	ConferenceID int IDENTITY(1,1) NOT NULL,
	Name nchar(256) NOT NULL,
	Starts date NOT NULL,
	Ends date NOT NULL,

 	PRIMARY KEY (ConferenceID)
);
ALTER TABLE dbo.Conference ADD CONSTRAINT check_date CHECK (Starts < Ends)

CREATE TABLE Event
(
	EventID int IDENTITY(1,1) NOT NULL,
	ConferenceID int NOT NULL,
	TotalPlaces int NOT NULL,
	Canceled bit NOT NULL DEFAULT 0,

	PRIMARY KEY(EventID),
	CONSTRAINT FK_Event_Conference FOREIGN KEY (ConferenceID) REFERENCES Conference(ConferenceID)
);
ALTER TABLE dbo.Event ADD CONSTRAINT check_places CHECK (TotalPlaces > 0)

CREATE TABLE ConferenceDay
(
	EventID int NOT NULL,
	Date date NOT NULL,

	PRIMARY KEY (EventID),
	CONSTRAINT FK_ConferenceDay_Event FOREIGN KEY (EventID) REFERENCES Event(EventID)
);

CREATE TABLE Workshop
(
	EventID int NOT NULL,
	Name nchar(128) NOT NULL,
	Starts time(7) NOT NULL,
	Ends time(7) NOT NULL,
	Price money NOT NULL,
	ConferenceDayID int NOT NULL,

	PRIMARY KEY (EventID),
	CONSTRAINT FK_Workshop_Event FOREIGN KEY (EventID) REFERENCES Event(EventID),
	CONSTRAINT FK_Workshop_ConferenceDay FOREIGN KEY (ConferenceDayID) REFERENCES ConferenceDay(EventID),
);
ALTER TABLE dbo.Workshop ADD CONSTRAINT check_time CHECK (Starts < Ends)
ALTER TABLE dbo.Workshop ADD CONSTRAINT check_workshop_price CHECK (Price > 0)

CREATE TABLE Prices
(
	PriceID int IDENTITY(1,1)NOT NULL,
	EventID int NOT NULL,
	Start date NOT NULL,
	Price money NOT NULL,

	PRIMARY KEY(PriceID),
	CONSTRAINT FK_Prices_ConferenceDay FOREIGN KEY (EventID) REFERENCES ConferenceDay(EventID)
);
ALTER TABLE dbo.Prices ADD CONSTRAINT check_price CHECK (Price > 0)

CREATE TABLE Customer
(
	CustomerID int IDENTITY(1,1) NOT NULL,
	Login nchar(50) NOT NULL,
	Phone nchar(20) NOT NULL,
	IsCompany bit NOT NULL,

	PRIMARY KEY(CustomerID)
);

CREATE TABLE Orders
(
	OrderID int IDENTITY(1,1)NOT NULL,
	CustomerID int NOT NULL,
	EventID int NOT NULL,
	OrderDate date NOT NULL DEFAULT GETDATE(),
	PlacesOrdered int NOT NULL,
	Canceled bit NOT NULL DEFAULT 0,

	PRIMARY KEY (OrderID),
	CONSTRAINT FK_Orders_Event FOREIGN KEY (EventID) REFERENCES Event(EventID),
	CONSTRAINT FK_Orders_Customer FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID)
);
ALTER TABLE dbo.Orders ADD CONSTRAINT check_placesordered CHECK (PlacesOrdered >= 0)

CREATE TABLE Participant
(
	ParticipantID int IDENTITY(1,1) NOT NULL,
	Name nchar(20) NOT NULL,
	Surname nchar(20) NOT NULL,
	StudentCard nchar(10),
	
	PRIMARY KEY(ParticipantID)
);

CREATE TABLE OrderDetails
(
	OrderDetailsID int IDENTITY(1,1) NOT NULL,
	OrderID int NOT NULL,
	ParticipantID int NOT NULL,
	CompanyName nchar(64),
	Removed bit NOT NULL DEFAULT 0,
	
	PRIMARY KEY(OrderDetailsID),
	CONSTRAINT FK_OrderDetails_Participant FOREIGN KEY (ParticipantID) REFERENCES Participant(ParticipantID),
	CONSTRAINT FK_OrderDetails_Orders FOREIGN KEY (OrderID) REFERENCES Orders(OrderID)
);

CREATE TABLE Payment
(
	PaymentID int IDENTITY(1,1) NOT NULL,
	OrderID int NOT NULL,
	PaymentDate date NOT NULL DEFAULT GETDATE(),
	Amount money NOT NULL,
	
	PRIMARY KEY(PaymentID),
	CONSTRAINT FK_Payment_Orders FOREIGN KEY (OrderID) REFERENCES Orders(OrderID)
);