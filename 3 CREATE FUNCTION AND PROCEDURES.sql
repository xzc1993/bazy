USE kzieba_a

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'AddReservation', N'P') IS NOT NULL
	DROP PROCEDURE AddReservation;
GO
CREATE PROCEDURE AddReservation
	@CustomerId int,
	@EventId int,
	@Seats int 
AS
BEGIN
	INSERT INTO Orders (CustomerID,EventId,PlacesOrdered) VALUES (@CustomerId,@EventId,@Seats)
END
GO

IF OBJECT_ID (N'AddReservationParticipant ', N'P') IS NOT NULL
	DROP PROCEDURE AddReservationParticipant ;
GO
CREATE PROCEDURE AddReservationParticipant 
	@ParticipantId int,
	@OrderID int
AS
BEGIN
	INSERT INTO OrderDetails (ParticipantId,OrderId) VALUES (@ParticipantId,@OrderID)
END
GO

IF OBJECT_ID (N'createConference', N'P') IS NOT NULL
    DROP PROCEDURE createConference;
GO
CREATE PROCEDURE createConference
(
	@Name nchar(50),
	@Start date,
	@End date,
	@Places dbo.PlacesAmount Readonly
)
AS
BEGIN
	SET NOCOUNT ON;

	Declare @cidTable table (id int)
	--Adding new Conference
	Insert Into Conference (Name,Starts,Ends) 
	Output inserted.ConferenceID Into @cidTable 
	values (@Name,@Start,@End)
	
	Declare @count int = 1
	--Getting Id of inserted conference
	Declare @cid int = (Select Top 1 id from @cidTable)
	--Inserting Events and ConferenceDays
	while(@Start <= @End)
	Begin
		Insert into Event 
		Values( @cid,(Select Amount from @Places where DayId = @count), 0)
		
		Insert into ConferenceDay (EventID,Date) values (SCOPE_IDENTITY(),@Start)
		Set @count = @count +1
		Set @Start = DATEADD(DAY,1,@Start)
	End

END
GO

IF OBJECT_ID (N'addParticipant', N'P') IS NOT NULL
    DROP PROCEDURE addParticipant;
GO
CREATE PROCEDURE addParticipant
	@Name nchar(20),
	@Surname nchar(50),
	@StudentCard nchar(10) = null
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO Participant (Name,Surname,StudentCard)
	VALUES (@Name,@Surname,@StudentCard)
END
GO

IF OBJECT_ID (N'addThreshold', N'P') IS NOT NULL
    DROP PROCEDURE addThreshold;
GO
CREATE PROCEDURE addThreshold
	-- Add the parameters for the stored procedure here
	@EventID int,
	@Start Date,
	@Price Money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO Prices (Start,Price,EventID) VALUES (@Start,@Price,@EventID)
END
GO

IF OBJECT_ID (N'addUser', N'P') IS NOT NULL
    DROP PROCEDURE addUser;
GO
CREATE PROCEDURE addUser(@Name NCHAR(50), @Phone NCHAR(20), @Is_company BIT)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO Customer( Login, Phone, IsCompany)
		VALUES ( @Name,  @Phone, @Is_company);
END
GO

IF OBJECT_ID (N'AddWorkshop', N'P') IS NOT NULL
    DROP PROCEDURE AddWorkshop;
GO
Create Procedure AddWorkshop
	
	@Name nchar(128),
	@Start time,
	@End time,
	@Price money,
	@Places int,
	@ConferenceDayId int
	
As
Begin
	
	Declare @ConferenceID int = (Select ConferenceID from Event where EventID = @ConferenceDayId)
	
	Insert into Event (ConferenceID,TotalPlaces) values(@ConferenceID,@Places)									

	insert into Workshop (EventID,Starts,Ends,Price,ConferenceDayId,Name)
	values (SCOPE_IDENTITY(),@Start,@End,@Price,@ConferenceDayId,@Name)

End
GO

IF OBJECT_ID (N'CancelConference', N'P') IS NOT NULL
	DROP PROCEDURE CancelConference;
GO
CREATE PROCEDURE CancelConference
(
	@conferenceID int
)
AS
BEGIN
	UPDATE Event SET Canceled = 1 WHERE ConferenceID = @conferenceID
END
GO

IF OBJECT_ID (N'CancelConferenceDay', N'P') IS NOT NULL
	DROP PROCEDURE CancelConferenceDay;
GO
CREATE PROCEDURE CancelConferenceDay
(
	@conferenceDayID int
)
AS
BEGIN
	UPDATE Event SET Canceled = 1 WHERE EventID = ConferenceID
	UPDATE Event SET Canceled = 1 WHERE EventID IN 
		(SELECT EventID
			FROM Workshop
			WHERE ConferenceDayID = @conferenceDayID)
END
GO

IF OBJECT_ID (N'CancelEvent', N'P') IS NOT NULL
	DROP PROCEDURE CancelEvent;
GO
CREATE PROCEDURE CancelEvent
(
	@eventID int
)
AS
BEGIN
	UPDATE Event SET Canceled = 1 WHERE EventID = @eventID
END
GO

IF OBJECT_ID (N'ChangeBookedSeats', N'P') IS NOT NULL
	DROP PROCEDURE ChangeBookedSeats;
GO
CREATE PROCEDURE ChangeBookedSeats
	@orderId int,
	@NewNumber int
AS
BEGIN
	UPDATE Orders SET PlacesOrdered = @NewNumber
END
GO


IF OBJECT_ID (N'ChangeConferenceDayPrice', N'P') IS NOT NULL
	DROP PROCEDURE ChangeConferenceDayPrice;
GO
CREATE PROCEDURE ChangeConferenceDayPrice
(
	@priceID int,
	@newPrice money
)
AS
BEGIN
	UPDATE Prices SET Price = @newPrice WHERE PriceID = @priceID
END
GO


IF OBJECT_ID (N'ChangeWorkshopPrice', N'P') IS NOT NULL
	DROP PROCEDURE ChangeWorkshopPrice;
GO
CREATE PROCEDURE ChangeWorkshopPrice
(
	@workshopID int, --EventID column
	@newPrice money
)
AS
BEGIN
	UPDATE Workshop SET Price = @newPrice WHERE EventID = @workshopID
END
GO

IF OBJECT_ID (N'registerPayment', N'P') IS NOT NULL
    DROP PROCEDURE registerPayment;
GO
CREATE PROCEDURE registerPayment(@orderID int, @amount money)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO Payment (OrderID, Amount)
	VALUES (@orderID, @amount)
END
GO

IF OBJECT_ID (N'AddReservation', N'P') IS NOT NULL
	DROP PROCEDURE AddReservation;
GO
CREATE PROCEDURE AddReservation
	@CustomerId int,
	@EventId int,
	@Seats int 
AS
BEGIN
	INSERT INTO Orders (CustomerID,EventId,PlacesOrdered) VALUES (@CustomerId,@EventId,@Seats)
END
GO

IF OBJECT_ID (N'RemoveParticipant', N'P') IS NOT NULL
	DROP PROCEDURE RemoveParticipant;
GO
CREATE PROCEDURE RemoveParticipant 
	@ParticipantId int,
	@OrderID int
AS
BEGIN
	UPDATE OrderDetails SET Removed = 1 WHERE OrderID = @OrderID AND ParticipantID = @ParticipantId
END
GO
----------------------------------------------------------------------------
--								FUNCTIONS
IF OBJECT_ID ('getStudentOrderHeadCount', N'FN') IS NOT NULL
	DROP FUNCTION getStudentOrderHeadCount
GO
CREATE FUNCTION getStudentOrderHeadCount
(
	@orderID int
)
RETURNS int
AS
BEGIN
	DECLARE @out int = 0
	SELECT @out = COUNT(*)
	FROM Orders AS O
	JOIN OrderDetails AS OD
		ON OD.OrderID = O.OrderID AND OD.Removed = 0
	JOIN Participant AS P
		ON P.ParticipantID = OD.ParticipantID AND P.StudentCard IS NOT NULL
	RETURN @out
END
GO

IF OBJECT_ID ('getAdultOrderHeadCount', N'FN') IS NOT NULL
	DROP FUNCTION getAdultOrderHeadCount
GO
CREATE FUNCTION getAdultOrderHeadCount
(
	@orderID int
)
RETURNS int
AS
BEGIN
	DECLARE @out int = 0
	SELECT @out = COUNT(*)
	FROM Orders AS O
	JOIN OrderDetails AS OD
		ON OD.OrderID = O.OrderID AND OD.Removed = 0
	JOIN Participant AS P
		ON P.ParticipantID = OD.ParticipantID AND P.StudentCard IS NULL
	RETURN @out
END
GO

IF OBJECT_ID ('getTotalOrderHeadCount', N'FN') IS NOT NULL
	DROP FUNCTION getTotalOrderHeadCount
GO
CREATE FUNCTION getTotalOrderHeadCount
(
	@orderID int
)
RETURNS int
AS
BEGIN
	DECLARE @out int = 0
	SELECT @out = COUNT(*)
	FROM Orders AS O
	JOIN OrderDetails AS OD
		ON OD.OrderID = O.OrderID AND OD.Removed = 0
	JOIN Participant AS P
		ON P.ParticipantID = OD.ParticipantID 
	RETURN @out
END
GO

IF OBJECT_ID ('getEventPrice', N'FN') IS NOT NULL
	DROP FUNCTION getEventPrice
GO
CREATE FUNCTION getEventPrice
(
	@eventID int,
	@date date
)
RETURNS money
AS
BEGIN
	DECLARE @out money = 0
	SET @out = 
	(SELECT Price
	FROM Workshop
	WHERE EventID = @eventID)
	IF @out IS NULL
		SET @out = 
		(SELECT TOP 1 Price
		FROM Prices
		WHERE EventID = @eventID AND Start < @date
		ORDER BY Start DESC)
	RETURN @out
END
GO

IF OBJECT_ID ('getTotalOrderPrice', N'FN') IS NOT NULL
	DROP FUNCTION getTotalOrderPrice
GO
CREATE FUNCTION getTotalOrderPrice
(
	@orderID int
)
RETURNS int
AS
BEGIN
	DECLARE @adult int
	DECLARE @student int
	DECLARE @price int
	DECLARE @eventID int
	DECLARE @orderDate date
	DECLARE @out money = 0
	
	SELECT @adult = dbo.getAdultOrderHeadCount(@orderID)
	SELECT @student = dbo.getStudentOrderHeadCount(@orderID)
	SELECT @eventID = EventID, @orderDate = OrderDate FROM Orders WHERE OrderID = @orderID
	SELECT @price = dbo.getEventPrice(@eventID, @orderDate)
	SET @out = @price * @adult + 0.8 * @price * @student
	RETURN @out
END
GO

IF OBJECT_ID ('getOrderSaldo', N'FN') IS NOT NULL
	DROP FUNCTION getOrderSaldo
GO
CREATE FUNCTION getOrderSaldo
(
	@orderID int
)
RETURNS int
AS
BEGIN
	DECLARE @price money = 0
	DECLARE @paid money = 0
	DECLARE @out money = 0
	
	SELECT @price = dbo.getTotalOrderPrice(@orderID)
	SELECT @paid = SUM(Amount)
	FROM Payment
	WHERE OrderID = @orderID
	IF @paid IS NULL
		SET @paid = 0
	SET @out = @price - @paid
	RETURN @paid
END
GO

IF OBJECT_ID ('getFreePlacesEvent', N'FN') IS NOT NULL
	DROP FUNCTION getFreePlacesEvent
GO
CREATE FUNCTION getFreePlacesEvent
(
	@eventID int
)
RETURNS int
AS
BEGIN
	DECLARE @out int
	SELECT @out = TotalPlaces
	FROM Event
	WHERE EventID = @eventID
	
	SELECT @out -= SUM(PlacesOrdered)
	FROM Orders
	WHERE EventID = @eventID

	RETURN @out
END
GO

IF OBJECT_ID (N'getEventParticipantList') IS NOT NULL
    DROP FUNCTION getEventParticipantList;
GO
CREATE FUNCTION getEventParticipantList(
	@event int
)
RETURNS @out TABLE
(
	Name nchar(20) NOT NULL,
	Surname nchar(20) NOT NULL,
	StudentCard nchar(10) NOT NULL,
	CompanyName nchar(40)
)
AS
BEGIN
	INSERT INTO @out (Name, Surname, StudentCard, CompanyName)
	SELECT P.Name, P.Surname, P.StudentCard, (CASE OD.CompanyName WHEN NULL THEN '' ELSE OD.CompanyName END)
	FROM Orders AS O
	JOIN OrderDetails AS OD
		ON O.OrderID = OD.OrderID AND OD.Removed = 0
	JOIN Participant AS P
		ON P.ParticipantID = OD.ParticipantID
	WHERE O.Canceled = 0 AND O.EventID = @event
	RETURN
END
GO

IF OBJECT_ID (N'getPaymentSummary') IS NOT NULL
    DROP FUNCTION getPaymentSummary;
GO
CREATE FUNCTION getPaymentSummary(
	@customer int
)
RETURNS @out TABLE
(
	EventID int NOT NULL,
	Price money NOT NULL,
	Paid money NOT NULL
)
AS
BEGIN
	INSERT INTO @out (EventID, Price, Paid)
	SELECT O.EventID, (SELECT dbo.getTotalOrderPrice(O.OrderID)), (SELECT dbo.getOrderSaldo(O.OrderID))
	FROM Customer AS C
	JOIN Orders AS O
		ON C.CustomerID = O.CustomerID AND O.Canceled = 0
	RETURN
END
GO

IF OBJECT_ID ('GetCustomerOrdersList') IS NOT NULL
    DROP FUNCTION GetCustomerOrdersList;
GO
CREATE FUNCTION GetCustomerOrdersList(
	@customerID int
)
RETURNS @out TABLE
(
	OrderID int NOT NULL,
	OrderedPlaces int NOT NULL,
	Price money NOT NULL,
	Paid money NOT NULL
)
AS
BEGIN
	INSERT INTO @out (OrderID, OrderedPlaces, Price, Paid)
	SELECT O.OrderID, O.PlacesOrdered, (SELECT dbo.getTotalOrderPrice(O.OrderID)), SUM(P.Amount)
	FROM Orders AS O
	JOIN Payment AS P
		ON P.OrderID = O.OrderID
	WHERE O.CustomerID = @customerID
	GROUP BY O.OrderID, O.PlacesOrdered
	RETURN
END