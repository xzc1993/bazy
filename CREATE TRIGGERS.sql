USE kzieba_a
GO
--PaymentTable
--ParticipantTable
--PricesTable
--CustomerTable

--OrdersTable
IF OBJECT_ID (N'EventCustomerChangeTrigger', N'TR') IS NOT NULL
	DROP TRIGGER EventCustomerChangeTrigger
GO
CREATE TRIGGER EventCustomerChangeTrigger ON dbo.Orders
	FOR UPDATE
AS
BEGIN
	IF UPDATE(EventID) OR UPDATE(CustomerID)
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR( 'EventCustomerChangeTrigger: EventID and CustomerID cannot be changed!', 16, 1)
	END
END
GO

IF OBJECT_ID (N'OrderZeroPlacesReservedTrigger', N'TR') IS NOT NULL
	DROP TRIGGER OrderZeroPlacesReservedTrigger
GO
CREATE TRIGGER OrderZeroPlacesReservedTrigger ON dbo.Orders
	FOR UPDATE
AS
BEGIN
	UPDATE Orders
	SET Canceled = 1
	WHERE OrderID IN(
		SELECT OrderID
			FROM inserted
			WHERE PlacesOrdered = 0)
END
GO

IF OBJECT_ID (N'OrderCanceledTrigger', N'TR') IS NOT NULL
	DROP TRIGGER OrderCanceledTrigger
GO
CREATE TRIGGER OrderCanceledTrigger ON dbo.Orders
	FOR UPDATE
AS
BEGIN
	IF UPDATE(Canceled)
		UPDATE OrderDetails
		SET Removed = 1
		WHERE OrderID IN(
			SELECT inserted.OrderID
				FROM inserted
				JOIN deleted
					ON inserted.OrderID = deleted.OrderID
				WHERE inserted.Canceled = 1 AND deleted.Canceled = 0)
END
GO

IF OBJECT_ID(N'EventCanceledNoReservationAllowedTrigger', N'TR') IS NOT NULL
	DROP TRIGGER EventCanceledNoReservationAllowedTrigger;
GO
CREATE TRIGGER EventCanceledNoReservationAllowedTrigger ON dbo.Orders
	FOR INSERT
AS
BEGIN
	IF (SELECT COUNT(*)
		FROM inserted AS i
		JOIN Event AS E
			ON i.EventID = E.EventID
		WHERE E.Canceled = 1) != 0
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR( 'EventCanceledNoReservationAllowedTrigger: Event was canceled', 16, 1)
	END
END
GO

IF OBJECT_ID (N'NoPlacesLeftTrigger', N'TR') IS NOT NULL
	DROP TRIGGER NoPlacesLeftTrigger
GO
CREATE TRIGGER NoPlacesLeftTrigger ON dbo.Orders
	FOR UPDATE
AS
BEGIN
	IF UPDATE(PlacesOrdered)
	BEGIN
		DECLARE @eventID int
		DECLARE @places int
		DECLARE @cursor cursor
		
		SET @cursor = CURSOR FAST_FORWARD
		FOR (SELECT DISTINCT EventID, PlacesOrdered
			FROM inserted)
		OPEN @cursor
		
		FETCH NEXT FROM @cursor
		INTO @eventID, @places
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (SELECT SUM(PlacesOrdered)
				FROM Orders
				WHERE EventID = @eventID) + @places > (SELECT TotalPlaces
														FROM Event
														WHERE EventID = @eventID)
			BEGIN
				ROLLBACK TRANSACTION
				RAISERROR( 'NoPlacesLeftTrigger: There is not enough seats left fotr reservation!', 16, 1)
			END
			FETCH NEXT FROM @cursor
			INTO @eventID, @places
		END
	END
END
GO

--OrderDetailsTable
IF OBJECT_ID (N'OrderIDChangeTrigger', N'TR') IS NOT NULL
	DROP TRIGGER OrderIDChangeTrigger
GO
CREATE TRIGGER OrderIDChangeTrigger ON dbo.OrderDetails
	FOR UPDATE
AS
BEGIN
	IF UPDATE(OrderID)
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR( 'OrderIDChangeTrigger: OrderID cannot be changed!', 16, 1)
	END
END
GO

IF OBJECT_ID (N'OrderDetailsParticipantRemovedTrigger', N'TR') IS NOT NULL
	DROP TRIGGER OrderDetailsParticipantRemovedTrigger
GO
CREATE TRIGGER OrderDetailsParticipantRemovedTrigger ON dbo.OrderDetails
	FOR UPDATE
AS
BEGIN
	DECLARE @cursor cursor
	DECLARE @orderID int
	DECLARE @placesRemoved int
	
	SET @cursor = CURSOR FAST_FORWARD
	FOR SELECT inserted.OrderID, Count(*)
		FROM inserted
		JOIN deleted
			ON inserted.Removed = 1 AND deleted.Removed = 0
		GROUP BY inserted.OrderID
		
	OPEN @cursor
	FETCH NEXT FROM @cursor
		INTO @orderID, @placesRemoved
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE Orders
		SET PlacesOrdered -= @placesRemoved
		WHERE OrderID = @orderID
	END
END
GO

IF OBJECT_ID (N'MultipleParticipantTrigger', N'TR') IS NOT NULL
	DROP TRIGGER MultipleParticipantTrigger
GO
CREATE TRIGGER MultipleParticipantTrigger ON dbo.OrderDetails
	FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @cursor cursor
	DECLARE @orderID int
	DECLARE @participant int
	
	SET @cursor = CURSOR FAST_FORWARD
	FOR SELECT inserted.OrderID, inserted.ParticipantID
	FROM inserted
	JOIN deleted
		ON inserted.OrderDetailsID = deleted.OrderDetailsID
		AND inserted.ParticipantID != deleted.ParticipantID
	OPEN @cursor
	
	FETCH NEXT FROM @cursor
	INTO @orderID, @participant
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (SELECT COUNT(*)
			FROM OrderDetails AS OD
			WHERE OD.OrderID = @orderID
			AND OD.ParticipantID = @participant) != 0
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR ( 'MultipleParticipantTrigger: Cannot assign the same participant twice to the same order!', 16, 1)
		END
		FETCH NEXT FROM @cursor
		INTO @orderID, @participant
	END
END
GO

IF OBJECT_ID (N'IsParticipantAllowedTrigger', N'TR') IS NOT NULL
	DROP TRIGGER IsParticipantAllowedTrigger
GO
CREATE TRIGGER IsParticipantAllowedTrigger ON dbo.OrderDetails
	FOR INSERT
AS
BEGIN
	DECLARE @cursor cursor
	DECLARE @participant int
	DECLARE @conferenceDay int
	DECLARE @workshop int
	DECLARE @start time
	DECLARE @end time
	
	SET @cursor = CURSOR FAST_FORWARD
	--For each added participant we assign conferenceDay he must belong to.
	FOR SELECT i.ParticipantID, W.ConferenceDayID, O.EventID
	FROM inserted AS i
	JOIN Orders AS O
		ON i.OrderID = O.OrderID AND O.Canceled = 0
	--It is ok, if we add participant of conferenceDay, we also don't have to check collsions.
	JOIN Workshop AS W
		ON O.EventID = W.EventID

	OPEN @cursor
	
	FETCH NEXT FROM @cursor
	INTO @participant, @conferenceDay, @workshop
	WHILE @@FETCH_STATUS = 0
	BEGIN	
		--Check if required conferenceDay is in actual particapnt's events.
		IF @conferenceDay NOT IN 
			(SELECT O.EventID
			FROM Orders AS O
			JOIN OrderDetails AS OD
				ON O.OrderID = OD.OrderID AND OD.ParticipantID = @participant AND OD.Removed = 0
				AND O.Canceled = 0)
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR( 'NoConferenceDayBookedTrigger: There is no reservation for conference day, which includes workshop!', 16, 1)
		END
		
		SELECT @end = ends, @start = starts
		FROM Workshop
		WHERE EventID = @workshop
		
		IF (SELECT COUNT(*)
			FROM Orders AS O
			JOIN OrderDetails AS OD
				ON O.OrderID = OD.OrderID AND OD.ParticipantID = @participant AND OD.Removed = 0
				AND O.Canceled = 0
			--We are interested only in workshops
			JOIN Workshop AS W
				ON O.EventID = W.EventID AND W.EventID != @workshop AND W.ConferenceDayID = @conferenceDay 
			WHERE ((W.Starts < @start AND W.Ends <= @start) OR
				  (W.Starts >= @end AND W.Ends > @end))) != 0
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR( 'NoConferenceDayBookedTrigger: Workshop collide with another one!', 16, 1)
		END
		FETCH NEXT FROM @cursor
		INTO @participant, @conferenceDay, @workshop
	END

END
GO

--EventTable
IF OBJECT_ID (N'EventCanceledTrigger', N'TR') IS NOT NULL
	DROP TRIGGER EventCanceledTrigger
GO
CREATE TRIGGER EventCanceledTrigger ON dbo.Event
	FOR UPDATE
AS
BEGIN
	IF UPDATE (Canceled)
	BEGIN
		UPDATE Orders
		SET Canceled = 1
		WHERE Orders.EventID IN(
			SELECT inserted.EventID
			FROM inserted
			JOIN deleted
				ON inserted.Canceled = 1 AND deleted.Canceled = 0)
	END
END
GO

IF OBJECT_ID (N'EventTotalPlacesChangeTrigger', N'TR') IS NOT NULL
	DROP TRIGGER EventTotalPlacesChangeTrigger
GO
CREATE TRIGGER EventTotalPlacesChangeTrigger ON dbo.Event
	FOR UPDATE
AS
BEGIN
	IF UPDATE (TotalPlaces)
	BEGIN
		IF (SELECT COUNT(*)
		FROM (SELECT inserted.EventID AS 'EventID', inserted.TotalPlaces AS 'Total', SUM(O.PlacesOrdered) AS 'Reserved'
			FROM inserted
			JOIN Orders AS O
				ON inserted.EventID = O.EventID
			GROUP BY inserted.EventID, inserted.TotalPlaces) AS I
		WHERE I.Total < I.Reserved) != 0
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR( 'EventTotalPlacesChangeTrigger: Too many seats already ordered!', 16, 1)
		END
	END
END
GO

--ConferenceTable
IF OBJECT_ID (N'ConferenceDateChangeTrigger', N'TR') IS NOT NULL
	DROP TRIGGER ConferenceDateChangeTrigger
GO
CREATE TRIGGER ConferenceDateChangeTrigger ON dbo.Conference
	FOR UPDATE
AS
BEGIN
	IF UPDATE(Starts) OR UPDATE(Ends)
	IF (SELECT COUNT(*)
		FROM (SELECT COUNT(*) AS 'Orders'
			FROM Conference AS C
			JOIN Event AS E
				ON E.ConferenceID = C.ConferenceID
			JOIN Orders AS O
				ON O.EventID = E.EventID
			WHERE C.ConferenceID IN( SELECT ConferenceID
									FROM inserted)
			GROUP BY C.ConferenceID) AS I
		WHERE I.Orders > 0) != 0
	BEGIN
		ROLLBACK TRANSACTION
		RAISERROR( 'ConferenceDateChangeTrigger: Orders on conference exists!', 16, 1)
	END
END
GO

--ConferenceDayTable
IF OBJECT_ID (N'ConferenceDayRescheduleTrigger', N'TR') IS NOT NULL
    DROP TRIGGER ConferenceDayRescheduleTrigger;
GO
CREATE TRIGGER ConferenceDayRescheduleTrigger ON dbo.ConferenceDay 
	FOR UPDATE
AS 
BEGIN
	IF UPDATE(Date)
	BEGIN
		IF (SELECT COUNT(*)
		    FROM(SELECT COUNT(*) AS 'Total'
				FROM Orders
				WHERE EventID IN(
					SELECT EventID
					FROM inserted)
				GROUP BY EventID) AS I
			WHERE I.Total > 0) != 0
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR( 'ConferenceDayRescheduleTrigger: Cannot change conference day!', 16, 1)
		END
	END
END
GO

--WorkshopTable
IF OBJECT_ID (N'WorkshopRescheduleTrigger', N'TR') IS NOT NULL
    DROP TRIGGER WorkshopRescheduleTrigger;
GO
CREATE TRIGGER WorkshopRescheduleTrigger ON dbo.Workshop
	FOR UPDATE
AS 
BEGIN
	IF UPDATE(ConferenceDayID) OR UPDATE(Starts) OR UPDATE(Ends)
	BEGIN
		IF (SELECT COUNT(*)
		    FROM(SELECT COUNT(*) AS 'Total'
				FROM Orders
				WHERE EventID IN(
					SELECT EventID
					FROM inserted)
				GROUP BY EventID) AS I
			WHERE I.Total > 0) != 0
		BEGIN
			ROLLBACK TRANSACTION
			RAISERROR( 'WorkshopRescheduleTrigger: Cannot change workshop!', 16, 1)
		END
	END
END
GO

