IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PlacesAmount' AND ss.name = N'dbo')
CREATE TYPE PlacesAmount AS TABLE
(
	DayId int IDENTITY(1,1) NOT NULL,
	EventId int NULL,
	Amount int NULL,
	
	PRIMARY KEY(DayID)
)

