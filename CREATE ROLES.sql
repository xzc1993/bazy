CREATE ROLE admin
CREATE ROLE customer
CREATE ROLE company
CREATE ROLE organizer

--Views for all
GRANT SELECT ON TopConference TO PUBLIC
GRANT SELECT ON TopWorkshop TO PUBLIC
GRANT SELECT ON getPaymentSummary TO PUBLIC

--Procedures for all
GRANT EXEC ON addParticipant TO PUBLIC
GRANT EXEC ON registerPayment TO PUBLIC
GRANT EXEC ON addUser TO PUBLIC
GRANT EXEC ON addReservationIndividual TO PUBLIC

--Functions for all
GRANT EXEC ON getFreePlacesEvent TO PUBLIC
GRANT EXEC ON getOrderSaldo TO PUBLIC
GRANT EXEC ON getEventPrice TO PUBLIC
GRANT EXEC ON getAdultOrderheadCount TO PUBLIC
GRANT EXEC ON getStudentOrderHeadCount TO PUBLIC
GRANT EXEC ON getTotalOrderHeadCount TO PUBLIC
GRANT EXEC ON getTotalOrderPrice TO PUBLIC
GRANT EXEC ON changeParticipant TO PUBLIC
GRANT EXEC ON removeParticipant TO PUBLIC

--Function for company
GRANT EXEC ON addReservation TO company, organizer, admin
GRANT EXEC ON addReservationParticipant TO company, organizer, admin

--Procedures for admin & organizer
GRANT EXEC ON addThreshold TO organizer, admin
GRANT EXEC ON addWorkshop TO organizer, admin
GRANT EXEC ON CancelConference TO organizer, admin
GRANT EXEC ON CancelConferenceDay TO organizer, admin
GRANT EXEC ON CancelEvent TO organizer, admin
GRANT EXEC ON ChangeBookedSeats TO organizer, admin
GRANT EXEC ON ChangeConferenceDayPrice TO organizer, admin
GRANT EXEC ON ChangeWorkshopPrice TO organizer, admin
GRANT EXEC ON createConference TO organizer, admin

--Views for admin & organizer
GRANT SELECT ON CompaniesToBePhoned TO organizer, admin
GRANT SELECT ON OrdersToBeCanceled TO organizer, admin
GRANT SELECT ON TrancationsToBeMade TO organizer, admin
GRANT SELECT ON TopCustomers TO organizer, admin

--Table-valued functions for admin & organizer
GRANT SELECT ON getEventParticipantList TO organizer, admin

--Admin can do anything
GRANT ALL PRIVILEGES ON Conference TO Admin
GRANT ALL PRIVILEGES ON ConferenceDay TO Admin
GRANT ALL PRIVILEGES ON Customer TO Admin
GRANT ALL PRIVILEGES ON Event TO Admin
GRANT ALL PRIVILEGES ON OrderDetails TO Admin
GRANT ALL PRIVILEGES ON Orders TO Admin
GRANT ALL PRIVILEGES ON Participant TO Admin
GRANT ALL PRIVILEGES ON Payment TO Admin
GRANT ALL PRIVILEGES ON Prices TO Admin
GRANT ALL PRIVILEGES ON Workshop TO Admin